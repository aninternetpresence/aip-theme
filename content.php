<?php
/*
 * Main content template. Much swiped from WP's twentyfifteen theme
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(['mb-5']); ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;

			$category_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'aip' ) );

			if( $category_list ){
				printf('<span class="category_list">%1$s</span>', $category_list);
			}

			$time_string = '<time class="entry-date published updated" datetime="%1$s" title="%2$s">%3$s</time>';

			if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
				$time_string = '<time class="entry-date published" datetime="%1$s" title="%2$s">%3$s</time>
								<span class="updated-on">
								  <span class="screen-reader-text updated-label">%7$s</span>
								  <time class="updated" datetime="%4$s" title="%6$s">%5$s</time>
								</span>';
			}

			$time_string = sprintf( $time_string,
					esc_attr( get_the_date( 'c' ) ),
					esc_attr( get_the_date( 'r' ) ),
					get_the_date(),
					esc_attr( get_the_modified_date( 'c' ) ),
					get_the_modified_date(),
					esc_attr( get_the_modified_date( 'r' ) ),
					_x( 'Updated: ', 'Used before updated date.', 'aip' )
					);

			printf( '<div class="posted-on">%1$s</div>', $time_string);
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			//$featured_image_size = 'medium_large';
			if( is_listing() ){
				$featured_image_size = [ 427, 240 ];
				// Only automatically show featured image on listings
				the_post_thumbnail( $featured_image_size, ['class' => 'img-fluid mb-3'] );

				/*
				 * Look for certain media in the blog post and feature it on the home page. This provides a way
				 * to feature media other than images.
				 *
				 * Featured media filter:
				 * <iframe>
				 * <img featured="true">
				 */

				/*
				 * Load the blog post into a DOMDocument object to search for a media element to feature
				 */
				$content = new DOMDocument();
				// Suppress HTML parsing errors
				$error_reporting = error_reporting( error_reporting() & ~E_WARNING) ;
				$content->loadHTML( get_the_content() );
				error_reporting( $error_reporting );
				$xpath = new DOMXPath( $content );
				$xp_query = '//*[@featured="true"]|//iframe';
				$found_media = $xpath->query( $xp_query );
				$found_media_count = $found_media->length;
				if( $found_media_count ){
					$element = $found_media->item(0);

					if( $element->hasAttribute( 'class' ) ){
                        $element->setAttribute( 'class', $element->getAttribute( 'class' ) . ' featured-medium' );
                    }

                    /*
					 * Remove width and height attributes from images to apply featured-* styles.
					 */
					if( strtoupper( $element->tagName ) === 'IMG' ){
						$element->removeAttribute( 'width' );
						$element->removeAttribute( 'height' );
						$displayHTML = $element->ownerDocument->saveHTML( $element );
					} else if(strtoupper( $element->tagName ) === 'IFRAME'){
					    $element->setAttribute('class', 'embed-responsive-item');
                        $displayHTML = '<div class="embed-responsive embed-responsive-16by9 mb-3">' . $element->ownerDocument->saveHTML( $element ) . '</div>';
                    } else {
                        $displayHTML = $element->ownerDocument->saveHTML( $element );
                    }
					echo $displayHTML;
				}
				the_excerpt();
			} else {
				the_content( sprintf(
					'Would you like to know more?',
					the_title( '<span class="screen-reader-text">', '</span>', false )
				) );
			}

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">Pages:</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">Page</span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( 'Edit', '<span class="edit-link">', '</span>' );

		if( ! is_listing() ){
			$tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'aip' ) );
			if ( $tags_list ) {
				printf( '<div class="tags-links"><span class="screen-reader-text">%1$s </span>%2$s</div>',
						_x( 'Tags: ', 'Used before tag names.', 'aip' ),
						$tags_list
						);
			}
		}
		?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
