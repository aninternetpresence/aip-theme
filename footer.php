<?php
/*
 * Main footer
 */
?>
		</div><!-- .site-content -->
        <footer id="footer">
            <?php
        the_posts_pagination( array(
            'prev_text'          => __( 'Previous page', 'aip' ),
            'next_text'          => __( 'Next page', 'aip' ),
            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'aip' ) . ' </span>',
            'screen_reader_text' => __( 'Posts' )
        ) );
        ?>
        </footer>
	</div><!-- .site -->
<?php wp_footer(); ?>
</body>
</html>