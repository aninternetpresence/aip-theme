<?php
add_theme_support( 'post-thumbnails' );
add_theme_support( 'infinite-scroll', array(
    'container' => 'content',
    'footer' => 'footer',
) );

if ( ! function_exists(  'is_listing' ) ){
	function is_listing(){
		return is_category() || is_archive() || is_home();
	}
}

/*
// Enable Maintenance Mode
function maintenace_mode() {
    if ( !current_user_can( 'administrator' ) ) {
        wp_die('AIP is currently under maintenance. Please check back in about.');
    }
}

add_action('get_header', 'maintenace_mode');
*/

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
			'name'          => 'Home right sidebar',
			'id'            => 'home_right_1',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );

function register_my_menu() {
	register_nav_menus([
			'header-menu' => __( 'Header Menu' ),
			'dev-guides-menu' => __( 'Dev Guides Menu' ),
			'amp-menu' => __( 'AMP Menu' )
	]);
}
add_action( 'init', 'register_my_menu' );

function honor_ssl_for_attachments($url) {
	$http = site_url(FALSE, 'http');
	$https = site_url(FALSE, 'https');
	return ( $_SERVER['HTTPS'] == 'on' ) ? str_replace($http, $https, $url) : $url;
}
add_filter('wp_get_attachment_url', 'honor_ssl_for_attachments');

function theme_styles() {
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css', [], null );

}
add_action( 'wp_enqueue_scripts', 'theme_styles');

function theme_js() {

    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', false, null);
    wp_enqueue_script('jquery');
    wp_enqueue_script( 'popper', 'https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js', [], null );
    wp_enqueue_script( 'bootstrap_js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js', [], null );
}
add_action( 'wp_enqueue_scripts', 'theme_js');