<?php
/*
 * Main header
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
	<div id="page" class="site">
		<header id="masthead" class="site-header" role="banner">
			<div class="site-branding ml-3"">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->
		</header>
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php
				// Primary navigation menu.
				wp_nav_menu( array(
					'menu_class'     => 'nav-menu header-menu',
					'theme_location' => 'header-menu'
				) );
			?>
		</nav><!-- .main-navigation -->
		<div id="sidebar" class="sidebar">
			<?php get_sidebar(); ?>
		</div><!-- .sidebar -->
		<div id="header-page-navigation">
            <?php
            the_posts_pagination( [
                'prev_text'          => __( 'Previous page', 'aip' ),
                'next_text'          => __( 'Next page', 'aip' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'aip' ) . ' </span>',
                'screen_reader_text' => __( 'Posts' )
            ] );
            ?>
        </div>
		<div id="content" class="site-content mt-4">