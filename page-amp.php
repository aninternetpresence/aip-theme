<?php
/**
 * Page template swiped from twentyfifteen
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">				
		<?php
		// Primary navigation menu.
		wp_nav_menu( array(
				'menu_class'     => 'nav-menu',
				'theme_location' => 'amp-menu'
		) );
		
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'content' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
