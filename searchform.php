<?php
/**
 * Template for displaying search forms, grabbed from Twenty Sixteen
 *
 */
?>

<form role="search" method="get" class="search-form m-4" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'aip' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'aip' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="search-submit">
		<span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'aip' ); ?></span>
		<?php echo _x( 'Search', 'submit button', 'aip' ); ?>
	</button>
</form>
