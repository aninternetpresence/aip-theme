<?php
/**
 * Template Name: Search Page
 */

global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
} //if

$search = new WP_Query($search_query);

get_header(); ?>

	<div id="primary" class="content-area">
		<h1>Search Posts</h1>
		<?php get_search_form(); ?>
	</div><!-- .content-area -->

<?php get_footer(); ?>
