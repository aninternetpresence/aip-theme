<?php
/*
 * Main sidebar. For now, only show the 'primary' menu
 */
if ( has_nav_menu( 'primary' ) ): ?>
	<div id="secondary" class="secondary">
	
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php
					// Primary navigation menu.
					wp_nav_menu( array(
						'menu_class'     => 'nav-menu',
						'theme_location' => 'primary',
					) );
				?>
			</nav><!-- .main-navigation -->
			<?php endif; ?>
	</div><!-- .secondary -->
				<?php endif; ?>
<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area d-flex justify-content-center flex-column flex-md-row" role="complementary">
		<?php dynamic_sidebar( 'home_right_1' ); ?>
	</div><!-- #primary-sidebar -->
<?php endif; ?>