<?php
/**
 * The template part for displaying results in search pages, swiped from Twenty Sixteen
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->

	<?php 
	if ( has_post_thumbnail() ) {
		$featured_image_id = get_post_thumbnail_id( $post->ID );
		$attachment_link = get_attachment_link( $featured_image_id );
		$attachment_data = wp_prepare_attachment_for_js( $featured_image_id );
		if ( ! empty( $attachment_link ) ) {
			echo '<a href="' . esc_url( $attachment_link ) . '" title="' . $attachment_data['title'] . '" target="_blank">';
			the_post_thumbnail( 'thumbnail' );
			echo '</a>';
		}
	}
	?>

	<?php the_excerpt(); ?>

	<?php if ( 'post' === get_post_type() ) : ?>

		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'aip' ),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->

	<?php else : ?>

		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'aip' ),
					get_the_title()
				),
				'<footer class="entry-footer"><span class="edit-link">',
				'</span></footer><!-- .entry-footer -->'
			);
		?>

	<?php endif; ?>
</article><!-- #post-## -->

